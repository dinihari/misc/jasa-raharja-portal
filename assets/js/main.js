$('.file-upload-browse').on('click', function(){
  var file = $(this).parent().parent().parent().find('.file-upload-default');
  file.trigger('click');
});

$('.file-upload-default').on('change', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});


$('.jsConfirmDelete').on('click', function(e){
  var url = $(this).data('url');
  var result = confirm("Apakah Anda yakin akan menghapus data ini?");
  if (result) {
    window.location.href = url;
  }
  e.preventDefault();
});