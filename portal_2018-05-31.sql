# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.21)
# Database: portal
# Generation Time: 2018-05-31 01:32:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table apps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `apps`;

CREATE TABLE `apps` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `flag` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `apps` WRITE;
/*!40000 ALTER TABLE `apps` DISABLE KEYS */;

INSERT INTO `apps` (`id`, `name`, `description`, `url`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`, `flag`)
VALUES
	('8ac932c2-6468-11e8-a85e-3c07546b6f32','Website Jasa Raharja',NULL,'https://jasaraharja.id','2018-05-31 00:21:20','80bd1159-eecc-4e49-a530-2cdd4d70bf00','2018-05-31 00:28:55','80bd1159-eecc-4e49-a530-2cdd4d70bf00',NULL,NULL),
	('9b0c7734-6468-11e8-ae74-3c07546b6f32','Human Resource Information System',NULL,'https://hris.jasa-raharja.id','2018-05-31 00:21:47','80bd1159-eecc-4e49-a530-2cdd4d70bf00',NULL,NULL,NULL,NULL),
	('abccb3fe-6468-11e8-a6ab-3c07546b6f32','E-Learning',NULL,'https://elearning.jasa-raharja.id','2018-05-31 00:22:15','80bd1159-eecc-4e49-a530-2cdd4d70bf00',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `apps` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table apps_media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `apps_media`;

CREATE TABLE `apps_media` (
  `app_id` varchar(255) NOT NULL DEFAULT '',
  `media_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `apps_media` WRITE;
/*!40000 ALTER TABLE `apps_media` DISABLE KEYS */;

INSERT INTO `apps_media` (`app_id`, `media_id`)
VALUES
	('8ac932c2-6468-11e8-a85e-3c07546b6f32','jasaraharja/ulczohcf2i7cml75udji'),
	('9b0c7734-6468-11e8-ae74-3c07546b6f32','jasaraharja/btvfslhuxhsbgucpgbps'),
	('abccb3fe-6468-11e8-a6ab-3c07546b6f32','jasaraharja/ufiwqkir3qvht9xfcr7f');

/*!40000 ALTER TABLE `apps_media` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table apps_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `apps_order`;

CREATE TABLE `apps_order` (
  `app_id` varchar(255) NOT NULL DEFAULT '',
  `order` int(255) DEFAULT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `apps_order` WRITE;
/*!40000 ALTER TABLE `apps_order` DISABLE KEYS */;

INSERT INTO `apps_order` (`app_id`, `order`)
VALUES
	('4df86454-f328-41d8-a33f-210c96502427',1);

/*!40000 ALTER TABLE `apps_order` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table logins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `logins`;

CREATE TABLE `logins` (
  `user_id` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `logins` WRITE;
/*!40000 ALTER TABLE `logins` DISABLE KEYS */;

INSERT INTO `logins` (`user_id`, `password`, `last_login`, `status`)
VALUES
	('80bd1159-eecc-4e49-a530-2cdd4d70bf00','21232f297a57a5a743894a0e4a801fc3',NULL,1);

/*!40000 ALTER TABLE `logins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `public_id` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`public_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;

INSERT INTO `media` (`public_id`, `url`)
VALUES
	('jasaraharja/btvfslhuxhsbgucpgbps','http://res.cloudinary.com/ypxdev/image/upload/v1527726109/jasaraharja/btvfslhuxhsbgucpgbps.jpg'),
	('jasaraharja/fa2kl4vvs0uelmp8gol1','http://res.cloudinary.com/ypxdev/image/upload/v1527725994/jasaraharja/fa2kl4vvs0uelmp8gol1.jpg'),
	('jasaraharja/t36syqadcpasxfk8x189','http://res.cloudinary.com/ypxdev/image/upload/v1527725892/jasaraharja/t36syqadcpasxfk8x189.jpg'),
	('jasaraharja/ufiwqkir3qvht9xfcr7f','http://res.cloudinary.com/ypxdev/image/upload/v1527726137/jasaraharja/ufiwqkir3qvht9xfcr7f.jpg'),
	('jasaraharja/ulczohcf2i7cml75udji','http://res.cloudinary.com/ypxdev/image/upload/v1527726082/jasaraharja/ulczohcf2i7cml75udji.jpg'),
	('jasaraharja/urday2dleflln0zdxjcm','http://res.cloudinary.com/ypxdev/image/upload/v1527441849/jasaraharja/urday2dleflln0zdxjcm.png'),
	('jasaraharja/wmtscwqpgmkcrrykmpto','http://res.cloudinary.com/ypxdev/image/upload/v1527441867/jasaraharja/wmtscwqpgmkcrrykmpto.png'),
	('jasaraharja/xxr6xf94a87xxmxxzpwu','http://res.cloudinary.com/ypxdev/image/upload/v1527724377/jasaraharja/xxr6xf94a87xxmxxzpwu.png');

/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `id` varchar(11) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `short` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;

INSERT INTO `modules` (`id`, `name`, `short`)
VALUES
	('0','User','users'),
	('1','Berita','news'),
	('2','Apps','apps'),
	('3','Dashboard','dashboard');

/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `flag` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;

INSERT INTO `news` (`id`, `title`, `content`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`, `flag`)
VALUES
	('56b57298-646d-11e8-8a63-3c07546b6f32','Terkenal Kasar di Lapangan, Ini 10 Bukti Sergio Ramos Family Man','Pertandingan laga final Liga Champions pada Minggu (27/5) yang mempertemukan Liverpool vs Real Madrid jadi momen penuh drama. Sergio Ramos, Kapten Real Madrid jadi hujatan warganet setelah mengalami benturan dengan Mohamed Salah. Tak ayal, pemain dari Mesir tersebut terpaksa harus keluar lapangan karena tak tahan menahan sakit di bahunya. Real madrid sendiri keluar sebagai juara setelah mengalahkan Liverpool dengan skor 3-1.','2018-05-31 00:55:40','80bd1159-eecc-4e49-a530-2cdd4d70bf00',NULL,NULL,NULL,NULL),
	('64a2cf68-646d-11e8-9064-3c07546b6f32','Jarang Diketahui Publik, Inilah Sosok Magi Salah, Istri Mohamed Salah yang Selalu Memberi Dukungans','Mohamed Salah (orang Inggris kerap menyingkat nama Mohamed menjadi Mo) memang menjadi pemain fenomenal di Liga Inggris musim ini.','2018-05-31 00:56:03','80bd1159-eecc-4e49-a530-2cdd4d70bf00','2018-05-31 01:01:19','80bd1159-eecc-4e49-a530-2cdd4d70bf00',NULL,NULL);

/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table news_media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `news_media`;

CREATE TABLE `news_media` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`)
VALUES
	(0,'Super Administrator','Role untuk akses semua module termasuk user management'),
	(1,'Administrator','Role untuk manage Berita dan Aplikasi'),
	(2,'User','Role biasa untuk lihat berita dan aplikasi');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles_modules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles_modules`;

CREATE TABLE `roles_modules` (
  `role_id` varchar(255) NOT NULL DEFAULT '',
  `module_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles_modules` WRITE;
/*!40000 ALTER TABLE `roles_modules` DISABLE KEYS */;

INSERT INTO `roles_modules` (`role_id`, `module_id`)
VALUES
	('0','0'),
	('0','1'),
	('0','2'),
	('0','3'),
	('1','1'),
	('1','2'),
	('1','3'),
	('2','3');

/*!40000 ALTER TABLE `roles_modules` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `npp` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `username`, `npp`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`)
VALUES
	('80bd1159-eecc-4e49-a530-2cdd4d70bf00','Super Admin','jr.super.admin','000000001','2018-05-24 07:41:25','SYSTEM',NULL,NULL,1),
	('80e833fe-61c8-11e8-aefe-3c07546b6f32','Dwinta Ayu',NULL,'908128379','2018-05-27 16:10:41','80bd1159-eecc-4e49-a530-2cdd4d70bf00',NULL,NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_roles`;

CREATE TABLE `users_roles` (
  `user_id` varchar(255) NOT NULL DEFAULT '',
  `role_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;

INSERT INTO `users_roles` (`user_id`, `role_id`)
VALUES
	('80bd1159-eecc-4e49-a530-2cdd4d70bf00','0'),
	('80e833fe-61c8-11e8-aefe-3c07546b6f32','1');

/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
