<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class JR_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->check_logged_in();
        
        if($this->get_user_session()) {
            $this->render_menu_sidebar();
        }
        
        $this->result = $this->init_result();

        $this->cur = null;
        $data['app_name'] = 'Jasa Raharja Workdesk';

        $this->load->vars($data);
    }

    public function set_cur($module)
    {
        $this->cur = $module;
        $data['cur'] = $this->cur;
        $this->load->vars($data);
    }

    public function init_cloudinary()
    {
        \Cloudinary::config(array( 
            "cloud_name" => "ypxdev", 
            "api_key" => "571656684939626", 
            "api_secret" => "5-aw90zB9Y_f0OsoCNFQ6w8hdJ4" 
        ));
    }

    public function get_current_timestamp()
    {
        return date('Y-m-d H:i:s');
    }

    public function render_menu_sidebar()
    {
        $menus = json_decode(file_get_contents(FCPATH . 'data/menu.json'));

        $this->load->model('M_Role', 'Role');
        $resultModules = $this->Role->GetModules($this->get_user_session()->role->id);
        $resultModulesArr = array_map(function($module){
            return $module->short;
        }, $resultModules);
        $data['menus'] = array_filter($menus, function($menu) use($resultModulesArr){
            if(in_array($menu->id, $resultModulesArr)) {
                return $menu;
            }
        });
        $this->load->vars($data);
    }

    public function get_user_session()
    {
        return $this->session->userdata('user');
    }

    public function exclude_uri()
    {
        return [
            'auth/actionLogout',
            'auth/actionLogin'
        ];
    }

    public function check_logged_in()
    {
        if(!in_array($this->uri->uri_string, $this->exclude_uri())) {
            $this->is_logged_in();
        }
    }

    public function is_logged_in()
    {
        if($this->session->userdata('user')) {
            $data['user'] = $this->session->userdata('user');
            $this->load->vars($data);
            if($this->uri->segment(1) == 'login') {
                redirect('dashboard');
            }
        } else {
            if($this->uri->segment(1) != 'login') {
                redirect('login');
            }
        }
        
    }

    public function init_datatables_assets()
    {
        $data['scripts'] = [
            'plugins' => [
                'star-admin/vendor/datatables.net/js/jquery.dataTables.js',
                'star-admin/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js',
                'js/datatables.js'
            ]
        ];
        $data['styles'] = [
            'plugins' => [
                'star-admin/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css'
            ]
        ];
        $this->load->vars($data);
    }

    public function response($data)
    {
        $response = $data->source;
        $cur = $data->default_path;
        if($response) {
            // debug($response);
            if($response->data != null) {
                $msg = @$data->state->success->msg;
                $path = @$data->state->success->path;
                $alert['type'] = 'success';
                $alert['msg'] = $msg ? $msg : 'Sukses'; 
            } else {
                $msg = @$data->state->error->msg;
                $path = @$data->state->error->path;
                $alert['type'] = 'danger';
                $alert['msg'] = $msg ? $msg : $response->error_message;
            }
            $this->session->set_flashdata('alert', set_message($alert));
            redirect($path ? $path : $cur);
        } else {
            $alert['type'] = 'danger';
            $alert['msg'] = 'Koneksi ke server gagal.';
            $this->session->set_flashdata('alert', set_message($alert));
            redirect($cur);
        }
        # code...
    }

    public function init_result()
    {
        $result = new stdClass();
        $result->data = null;
        $result->error_message = null;
        return $result;
        # code...
    }

    public function image_screening($files)
    {
        $files = (object)$files;
        $allowed_type = ['png', 'jpg', 'jpeg'];
        $type = explode('/', $files->type)[1];
        if($files->error or !in_array($type, $allowed_type)) {
            return false;
        } else {
            return true;
        }
    }

}

/* End of file JR_Controller.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/core/JR_Controller.php */