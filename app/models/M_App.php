<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_App extends JR_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = 'apps';
    }

    public function Get($id = null)
    {
        if($id) {
            $where = [
                'id' => $id
            ];
            $resultApp = $this->db->get_where($this->table, $where)->row();
            $resultApp->image = $this->GetImage($id);
            return $resultApp;
        } else {
            $resultApp = $this->db->get($this->table)->result();
            return array_map(function($app){
                $app->image = $this->GetImage($app->id);
                return $app;
            }, $resultApp);
        }
    }

    public function GetImage($appId)
    {
        $where = [
            'app_id' => $appId 
        ];
        $this->db->select('media.*');
        $this->db->from('apps_media');
        $this->db->join('media', 'media.public_id = apps_media.media_id');
        $this->db->where($where);
        return $this->db->get()->row();
    }

    public function Create($data)
    {
        $this->db->insert($this->table, $data);
        $this->result->data = true;
        return $this->result;
    }

    public function Update($data, $appId)
    {
        $where = [
            'id' => $appId
        ];
        $this->db->where($where);
        $this->db->set($data);
        $this->db->update('apps');

        return $this->ReturnStatus();
    }

    public function InsupMedia($appId, $mediaId)
    {
        $table = 'apps_media';
        $where = array('app_id', $appId);
        if($this->db->get_where($table, $where)->row()){
            $this->db->where($where);
            $this->db->set(array('media_id' => $mediaId));
            $this->db->update($table);
        } else {
            $data = array(
                'app_id' => $appId,
                'media_id' => $mediaId
            );
            $this->db->set($data);
            $this->db->insert($table);
        }
    }

    public function Delete($id)
    {
        $where = ['id' => $id];
        if($this->GetImage($id)) {
            $this->db->delete('apps_media', ['app_id' => $id]);
        }
        $this->db->delete($this->table, $where);
        return $this->ReturnStatus();
    }

}

/* End of file M_App.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/models/M_App.php */