<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Media extends JR_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = 'media';
    }

    public function Create($data)
    {
        $this->db->insert($this->table, $data);
    }

}

/* End of file M_Media.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/models/M_Media.php */