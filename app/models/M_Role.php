<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Role extends JR_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table = 'roles';
    }

    public function Get($roleId)
    {
        if($roleId) {
            $where = [
                'id' => $roleId
            ];
            return $this->db->get_where($this->table, $where)->row();
        } else {
            return $this->db->get($this->table)->result();
        }
    }

    public function GetModules($roleId)
    {
        $where = [
            'role_id' => $roleId
        ];
        $this->db->select('modules.*');
        $this->db->from('roles_modules');
        $this->db->join('modules', 'modules.id = roles_modules.module_id');
        $this->db->where($where);
        return $this->db->get()->result();
    }

}

/* End of file M_Role.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/models/M_Role.php */