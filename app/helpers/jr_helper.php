<?php

use Cocur\Slugify\Slugify;
use Carbon\Carbon;

function css($path)
{
    $url = assets_url($path);
    $css = '<link rel="stylesheet" href="'.$url.'">';
    echo $css;
}

function js($path)
{
    $url = assets_url($path);
    $js = '<script src="'.$url.'"></script>';
    echo $js;
}

function img($path)
{
    $url = assets_url($path);
    $img = '<img src="'.$url.'" />';
    echo $img;
}

function assets_url($path)
{
    return site_url('assets/' . $path);
}


function debug( $var )
{
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    exit;
}

function json_render($json)
{
    header("Access-Control-Allow-Origin: *");
    header('Content-Type: application/json');
    echo json_encode($json);
}

function set_message($alert)
{
    $type = $alert['type'];
    $msg = $alert['msg'];
    return '<div class="alert alert-'.$type.'">'.$msg.'</div>';
}

function placeholder($w, $h)
{
    return 'https://placehold.it/'.$w.'x'.$h;
}

function truncate($str, $length, $postfix = '...')
{
    if(strlen($str) > $length) {
        $truncated = substr($str, 0, strpos(wordwrap($str, $length), "\n"));
        return $truncated . $postfix;
    } else {
        return $str;
    }
}

function objectCast($arrays)
{
    return json_decode(json_encode($arrays));
}

function slugify($str)
{
    $slugify = new Slugify();   
    return $slugify->slugify($str);
}

function carbonFormat($date, $format)
{
    return $date = Carbon::parse($date)->format($format);
}

?>