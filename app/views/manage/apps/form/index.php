<?php if($this->session->flashdata('alert')) echo $this->session->flashdata('alert'); ?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">
          Form Tambah Aplikasi Baru
          <?php if($action == 'update'): ?>
            <a href="#" data-url="<?php echo base_url($cur . '/actionDelete/' . $app->id) ?>" class="jsConfirmDelete pull-right btn btn-outline-danger">
              Hapus Aplikasi Ini
            </a>
          <?php endif; ?>
        </h4>
        <?php $this->load->view($cur . '/form/form'); ?>
      </div>
    </div>
  </div>
</div>