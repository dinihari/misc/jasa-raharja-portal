<table class="table datatables" cellspacing="0">
  <thead>
    <tr>
      <th>Nama</th>
      <th>NPP</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($users as $key => $user): ?>
    <tr>
      <td><?php echo (@$user->name) ? $user->name : '-'  ?></td>
      <td><?php echo $user->npp ?></td>
      <td>
        <a href="<?php echo base_url($cur . '/actionDelete/') . $user->id ?>" class="btn btn-outline-danger">
          <i class="fa fa-trash"></i> Hapus
        </a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>