<?php if($this->session->flashdata('alert')) echo $this->session->flashdata('alert'); ?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">
          Form Tambah User Admin
        </h4>
        <?php $this->load->view($cur . '/form/form'); ?>
      </div>
    </div>
  </div>
</div>