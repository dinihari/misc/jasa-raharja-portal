<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <h2>Berita Terbaru</h2>
            <p class="text-muted">Berita terbaru seputar Jasa Raharja</p>
          </div>
        </div>
        <div class="row">
        <?php foreach ($news as $key => $n): ?>
          <div class="col-12 results">
            <div class="pt-4 border-bottom">
              <a class="d-block h4 mb-0" href="<?php echo base_url('dashboard/news/') . $n->id ?>"><?php echo $n->title ?></a>
              <p class="page-description mt-1 w-75 text-muted">
                by <?php echo $n->user->name ?> tanggal <?php echo carbonFormat($n->created_at, 'd M Y H:i:s') ?>
              </p>
            </div>
          </div>
        <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</div>