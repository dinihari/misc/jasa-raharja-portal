<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-12 mb-3">
            <a href="<?php echo base_url('dashboard') ?>" class="btn btn-outline-secondary">
              <i class="fa fa-long-arrow-left"></i> Kembali
            </a>           
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <h2><?php echo $news->title ?></h2>
            <p class="text-muted">
              by <?php echo $news->user->name ?> tanggal <?php echo carbonFormat($news->created_at, 'd M Y H:i:s') ?>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <p>
              <?php echo $news->content ?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>