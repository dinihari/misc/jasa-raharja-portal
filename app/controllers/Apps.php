<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;

class Apps extends JR_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->set_cur('manage/apps');
        $this->load->model('M_App', 'App');
        $this->load->model('M_Media', 'Media');
    }

    public function view_table()
    {
        $this->init_datatables_assets();
        $responseApps = $this->App->Get();
        if($responseApps) {
            $data['apps'] = $responseApps;
        } else {
            $data['apps'] = [];
        }
        $this->template->render($this->cur . '/view/index', $data);
    }

    public function view_form($appId = null)
    {
        if($appId == null) {
            $data['action'] = 'create';
        } else {
            $data['id'] = $appId;
            $data['app'] = $this->App->Get($appId);
            $data['action'] = 'update';
        }

        $this->template->render($this->cur . '/form/index', $data);
    }

    public function actionCreate()
    {
        $data['id'] = Uuid::uuid1()->toString();
        $data['name'] = $this->input->post('name');
        $data['url'] = $this->input->post('url');
        $data['created_at'] = $this->get_current_timestamp();
        $data['created_by'] = $this->get_user_session()->id;

        if($_FILES['image']['tmp_name']) {
            $resultImage = $this->image_screening($_FILES['image']);

            if(!$resultImage) {
                $alert['type'] = 'danger';
                $alert['msg'] = 'File terlalu besar / format file salah! pastikan file dibawah 2MB';
                $this->session->set_flashdata('alert', set_message($alert));
                redirect($this->cur . '/form/');
            }

            $this->init_cloudinary();
            $resultCloudinary = \Cloudinary\Uploader::upload(
                $_FILES['image']['tmp_name'], 
                array(
                    "folder" => "jasaraharja/"
                )
            );
            $mediaData['public_id'] = $resultCloudinary['public_id'];
            $mediaData['url'] = $resultCloudinary['url'];
            $this->Media->Create($mediaData);
            $this->App->InsupMedia($data['id'], $mediaData['public_id']);
        }

        $response = $this->App->Create($data);
        $responseData = [
            'source' => $response,
            'state' => [
                'success' => [
                    'msg' => 'Sukses menambahkan aplikasi baru'
                ],
                'error' => [
                    'path' => $this->cur . '/form'
                ]
            ],
            'default_path' => $this->cur
        ];
        $this->response(objectCast($responseData));
    }

    public function actionUpdate()
    {   
        $appId = $this->input->post('appId');
        $data['name'] = $this->input->post('name');
        $data['url'] = $this->input->post('url');
        $data['updated_at'] = $this->get_current_timestamp();
        $data['updated_by'] = $this->get_user_session()->id;


        if($_FILES['image']['tmp_name']) {
            $resultImage = $this->image_screening($_FILES['image']);

            if(!$resultImage) {
                $alert['type'] = 'danger';
                $alert['msg'] = 'File terlalu besar / format file salah! pastikan file dibawah 2MB';
                $this->session->set_flashdata('alert', set_message($alert));
                redirect($this->cur . '/form/' . $appId);
            }

            $this->init_cloudinary();
            $resultCloudinary = \Cloudinary\Uploader::upload(
                $_FILES['image']['tmp_name'], 
                array(
                    "folder" => "jasaraharja/"
                )
            );
            $mediaData['public_id'] = $resultCloudinary['public_id'];
            $mediaData['url'] = $resultCloudinary['url'];
            $this->Media->Create($mediaData);
            $this->App->InsupMedia($data['id'], $mediaData['public_id']);
        }

        $response = $this->App->Update($data, $appId);
        $responseData = [
            'source' => $response,
            'state' => [
                'success' => [
                    'msg' => 'Sukses mengupdate Aplikasi'
                ],
                'error' => [
                    'path' => $this->cur . '/form/' . $appId
                ]
            ],
            'default_path' => $this->cur
        ];
        $this->response(objectCast($responseData));
    }

    public function actionDelete($appId)
    {
        $response = $this->App->Delete($appId);
        $responseData = [
            'source' => $response,
            'state' => [
                'success' => [
                    'msg' => 'Sukses menghapus Aplikasi'
                ],
                'error' => [
                    'msg' => 'Gagal Menghapus Aplikasi'
                ]
            ],
            'default_path' => $this->cur
        ];
        $this->response(objectCast($responseData));
    }

}

/* End of file Apps.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/controllers/Apps.php */