<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends JR_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Auth', 'Auth');
        $this->load->model('M_User', 'User');
        $this->load->model('M_Role', 'Role');
    }

    public function index()
    {
        redirect('login');
    }

    public function viewLogin()
    {
        $this->template->render_auth('auth/login');
    }

    public function actionLogin()
    {
        $this->destroySession();        
        if(!$this->authREST($this->input->post())) {
            $this->authDB($this->input->post());
        }
    }

    public function authDB($data)
    {
        $resultUser = $this->User->Get('username', $data['username']);

        if(!$resultUser) {
            $alert['type'] = 'danger';
            $alert['msg'] = 'User tidak ditemukan!';
            $this->session->set_flashdata('alert', set_message($alert));
            redirect('login');
        }
        
        $resultLogin = $this->Auth->GetLogin($resultUser->id);
        
        if($resultLogin->password != md5($data['password'])) {
            $alert['type'] = 'danger';
            $alert['msg'] = 'Password Salah!';
            $this->session->set_flashdata('alert', set_message($alert));
            redirect('login');
        }

        $userData->id = $resultUser->id;
        $userData->name = $resultUser->name;
        $userData->role = $this->User->GetRole($resultUser->id);
        $this->initSession($userData);
        redirect('dashboard');
    }

    public function authREST($data)
    {
        $resultUserREST = $this->Auth->LoginREST($data);

        if($resultUserREST->status != 'error') {

            $userData = new stdClass();
            // match and get data with npp
            $resultUserDB = $this->User->GetByNPP($resultUserREST->users->login);

            if($resultUserDB) {
                // sync name from rest to local database
                $this->syncName($resultUserDB, $resultUserREST);
                // prepare for session
                $userData->id = $resultUserDB->id;
                $userData->name = $resultUserREST->users->name;
                $userData->role = $this->User->GetRole($resultUserDB->id);
                $this->initSession($userData);
            } else {
                // prepare for session
                $userData->id = $resultUserREST->users->login;
                $userData->name = $resultUserREST->users->name;
                $userData->role = $this->Role->Get(2);
                $this->initSession($userData);
            }
            redirect('dashboard');
        } else {
            return false;
        }
    }

    public function syncName($resultUserDB, $resultUserREST)
    {
        if($resultUserDB->name != $resultUserREST->users->name) {
            $data = [
                'name' => $resultUserREST->users->name
            ];
            $this->User->Update($data, $resultUserDB->id);
        }
    }

    public function initSession($userData)
    {
        $this->session->set_userdata('isLogin', true);
        $this->session->set_userdata('user', $userData);
    }

    public function destroySession()
    {
        // $this->session->sess_destroy();
    }

    public function actionLogout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}

/* End of file Auth.php */
/* Location: .//Users/yuri/Repositories/Misc/JasaRaharjaPortal/app/controllers/Auth.php */